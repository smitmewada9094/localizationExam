//
//  ViewController.swift
//  localizationExam 17-feb
//
//  Created by Smit mewada on 28/11/1944 Saka.
//

import UIKit

extension String {
    var localEnglish : String{
        get {
            return NSLocalizedString(self, tableName: "Localizable", comment: "")
        }
    }
    var localHindi : String{
        get {
            let x = Bundle.main
            let path = x.path(forResource: "hi", ofType: "lproj")!
            let bundle = Bundle(path: path)
            return NSLocalizedString(self, tableName: "Localizable", bundle: bundle!, comment: "")
        }
    }
}
    class ViewController: UIViewController {
        
        @IBOutlet weak var textTextField: UITextField!
        
        @IBOutlet weak var dataLabel: UILabel!
        override func viewDidLoad() {
            super.viewDidLoad()
        }
        
        @IBAction func onClickToShow(_ sender: Any) {
            let data = UserDefaults.standard.object(forKey: "select") as? Int ?? 0
            
            if data == 0 {
                switch textTextField.text {
                case "m" :
                    dataLabel.text = NSLocalizedString("m", comment: "")
                case "t":
                    dataLabel.text = NSLocalizedString("t", comment: "")
                case "w" :
                    dataLabel.text = NSLocalizedString("w", comment: "")
                case "T" :
                    dataLabel.text = NSLocalizedString("T", comment: "")
                case "f" :
                    dataLabel.text = NSLocalizedString("f", comment: "")
                case "s" :
                    dataLabel.text = NSLocalizedString("s", comment: "")
                case "S" :
                    dataLabel.text = NSLocalizedString("S", comment: "")
                default:
                    break
                }
//                dataLabel.text = "\(textTextField.text)".localEnglish
            }else if data == 1 {
                dataLabel.text = textTextField.text?.localEnglish
            }else if data == 2 {
                dataLabel.text = textTextField.text?.localHindi
            }
            
          /*
            switch textTextField.text {
            case "m" :
                dataLabel.text = NSLocalizedString("m", comment: "")
            case "t":
                dataLabel.text = NSLocalizedString("t", comment: "")
            case "w" :
                dataLabel.text = NSLocalizedString("w", comment: "")
            case "T" :
                dataLabel.text = NSLocalizedString("T", comment: "")
            case "f" :
                dataLabel.text = NSLocalizedString("f", comment: "")
            case "s" :
                dataLabel.text = NSLocalizedString("s", comment: "")
            case "S" :
                dataLabel.text = NSLocalizedString("S", comment: "")
            default:
                break
            }
*/
            }
        
    }

